#  This file is part of Lazylibrarian.
#  Lazylibrarian is free software':'you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#  Lazylibrarian is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  You should have received a copy of the GNU General Public License
#  along with Lazylibrarian.  If not, see <http://www.gnu.org/licenses/>.

import re
import time
import traceback
import unicodedata
import threading

try:
    import urllib3
    import requests
except ImportError:
    import lib.requests as requests

import lazylibrarian
from lazylibrarian import logger, database
from lazylibrarian.bookwork import getWorkSeries, getWorkPage, deleteEmptySeries, \
    setSeries, getStatus, isbn_from_words, thingLang, getBookPubdate, get_gb_info, \
    get_gr_genres, setGenres, genreFilter
from lazylibrarian.images import getBookCover
from lazylibrarian.cache import gr_xml_request, cache_img
from lazylibrarian.formatter import plural, today, replace_all, bookSeries, unaccented, split_title, getList, \
    cleanName, is_valid_isbn, formatAuthorName, check_int, makeUnicode, check_year, check_float, makeUTF8bytes
try:
    from fuzzywuzzy import fuzz
except ImportError:
    from lib.fuzzywuzzy import fuzz
# noinspection PyUnresolvedReferences
from six.moves.urllib_parse import quote, quote_plus, urlencode


class GoodReads:
    # https://www.goodreads.com/api/

    def __init__(self, name=None):
        self.name = makeUnicode(name)
        # self.type = type
        if not lazylibrarian.CONFIG['GR_API']:
            logger.warn('No Goodreads API key, check config')
        self.params = {"key": lazylibrarian.CONFIG['GR_API']}

    def find_results(self, searchterm=None, queue=None):
        # noinspection PyBroadException
        try:
            resultlist = []
            api_hits = 0
            searchtitle = ''
            searchauthorname = ''

            if ' <ll> ' in searchterm:  # special token separates title from author
                searchtitle, searchauthorname = searchterm.split(' <ll> ')
                searchterm = searchterm.replace(' <ll> ', ' ')
                searchtitle = searchtitle.split(' (')[0]  # without any series info

            url = quote_plus(makeUTF8bytes(searchterm)[0])
            set_url = '/'.join([lazylibrarian.CONFIG['GR_URL'],
                                'search.xml?q=' + url + '&' + urlencode(self.params)])
            logger.debug('Now searching GoodReads API with searchterm: %s' % searchterm)
            if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                logger.debug(set_url)

            resultcount = 0
            try:
                try:
                    rootxml, in_cache = gr_xml_request(set_url)
                except Exception as e:
                    logger.error("%s finding gr results: %s" % (type(e).__name__, str(e)))
                    queue.put(resultlist)
                    return
                if rootxml is None:
                    logger.debug("Error requesting results")
                    queue.put(resultlist)
                    return

                totalresults = check_int(rootxml.find('search/total-results').text, 0)

                resultxml = rootxml.iter('work')
                loopCount = 1
                while resultxml:
                    for author in resultxml:
                        try:
                            if author.find('original_publication_year').text is None:
                                bookdate = "0000"
                            elif check_year(author.find('original_publication_year').text, past=1800, future=0):
                                bookdate = author.find('original_publication_year').text
                                try:
                                    bookmonth = check_int(author.find('original_publication_month').text, 0)
                                    bookday = check_int(author.find('original_publication_day').text, 0)
                                    if bookmonth and bookday:
                                        bookdate = "%s-%02d-%02d" % (bookdate, bookmonth, bookday)
                                except (KeyError, AttributeError):
                                    pass
                            else:
                                bookdate = "0000"
                        except (KeyError, AttributeError):
                            bookdate = "0000"

                        try:
                            authorNameResult = author.find('./best_book/author/name').text
                            # Goodreads sometimes puts extra whitespace in the author names!
                            authorNameResult = ' '.join(authorNameResult.split())
                        except (KeyError, AttributeError):
                            authorNameResult = ""

                        booksub = ""
                        bookpub = ""
                        booklang = "Unknown"

                        try:
                            bookimg = author.find('./best_book/image_url').text
                            if not bookimg or 'nocover' in bookimg or 'nophoto' in bookimg:
                                bookimg = 'images/nocover.png'
                        except (KeyError, AttributeError):
                            bookimg = 'images/nocover.png'

                        try:
                            bookrate = check_float(author.find('average_rating').text, 0)
                        except KeyError:
                            bookrate = 0.0
                        try:
                            bookrate_count = check_int(author.find('ratings_count').text, 0)
                        except KeyError:
                            bookrate_count = 0

                        bookpages = '0'
                        bookgenre = ''
                        bookdesc = ''
                        bookisbn = ''
                        workid = ''

                        try:
                            booklink = '/'.join([lazylibrarian.CONFIG['GR_URL'],
                                                'book/show/' + author.find('./best_book/id').text])
                        except (KeyError, AttributeError):
                            booklink = ""

                        try:
                            authorid = author.find('./best_book/author/id').text
                        except (KeyError, AttributeError):
                            authorid = ""

                        try:
                            if author.find('./best_book/title').text is None:
                                bookTitle = ""
                            else:
                                bookTitle = author.find('./best_book/title').text
                        except (KeyError, AttributeError):
                            bookTitle = ""

                        if searchauthorname:
                            author_fuzz = fuzz.ratio(authorNameResult, searchauthorname)
                        else:
                            author_fuzz = fuzz.ratio(authorNameResult, searchterm)
                        if searchtitle:
                            if bookTitle.endswith(')'):
                                bookTitle = bookTitle.rsplit(' (', 1)[0]
                            book_fuzz = fuzz.token_set_ratio(bookTitle, searchtitle)
                            # lose a point for each extra word in the fuzzy matches so we get the closest match
                            words = len(getList(bookTitle))
                            words -= len(getList(searchtitle))
                            book_fuzz -= abs(words)
                        else:
                            book_fuzz = fuzz.token_set_ratio(bookTitle, searchterm)
                            words = len(getList(bookTitle))
                            words -= len(getList(searchterm))
                            book_fuzz -= abs(words)
                        isbn_fuzz = 0
                        if is_valid_isbn(searchterm):
                            isbn_fuzz = 100
                            bookisbn = searchterm

                        highest_fuzz = max((author_fuzz + book_fuzz) / 2, isbn_fuzz)

                        try:
                            bookid = author.find('./best_book/id').text
                        except (KeyError, AttributeError):
                            bookid = ""

                        # Don't query google for every book we find, it's too slow and too many
                        # api hits. Only query the ones we want to add to db later
                        # if not bookdesc:
                        #     bookdesc = get_book_desc(isbn=bookisbn, author=authorNameResult, title=bookTitle)
                        resultlist.append({
                            'authorname': authorNameResult,
                            'bookid': bookid,
                            'authorid': authorid,
                            'bookname': bookTitle,
                            'booksub': booksub,
                            'bookisbn': bookisbn,
                            'bookpub': bookpub,
                            'bookdate': bookdate,
                            'booklang': booklang,
                            'booklink': booklink,
                            'bookrate': bookrate,
                            'bookrate_count': bookrate_count,
                            'bookimg': bookimg,
                            'bookpages': bookpages,
                            'bookgenre': bookgenre,
                            'bookdesc': bookdesc,
                            'workid': workid,
                            'author_fuzz': author_fuzz,
                            'book_fuzz': book_fuzz,
                            'isbn_fuzz': isbn_fuzz,
                            'highest_fuzz': highest_fuzz
                        })

                        resultcount += 1

                    loopCount += 1

                    if 0 < lazylibrarian.CONFIG['MAX_PAGES'] < loopCount:
                        resultxml = None
                        logger.warn('Maximum results page search reached, still more results available')
                    elif totalresults and resultcount >= totalresults:
                        # fix for goodreads bug on isbn searches
                        resultxml = None
                    else:
                        URL = set_url + '&page=' + str(loopCount)
                        resultxml = None
                        if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                            logger.debug(set_url)
                        try:
                            rootxml, in_cache = gr_xml_request(URL)
                            if rootxml is None:
                                logger.debug('Error requesting page %s of results' % loopCount)
                            else:
                                resultxml = rootxml.iter('work')
                                if not in_cache:
                                    api_hits += 1
                        except Exception as e:
                            resultxml = None
                            logger.error("%s finding page %s of results: %s" % (type(e).__name__, loopCount, str(e)))

                    if resultxml:
                        if all(False for _ in resultxml):  # returns True if iterator is empty
                            resultxml = None

            except Exception as err:
                # noinspection PyUnresolvedReferences
                if hasattr(err, 'code') and err.code == 404:
                    logger.error('Received a 404 error when searching for author')
                # noinspection PyUnresolvedReferences
                elif hasattr(err, 'code') and err.code == 403:
                    logger.warn('Access to api is denied 403: usage exceeded')
                else:
                    logger.error('An unexpected error has occurred when searching for an author: %s' % str(err))
                    logger.error('in GR.find_results: %s' % traceback.format_exc())

            logger.debug('Found %s %s with keyword: %s' % (resultcount, plural(resultcount, "result"), searchterm))
            logger.debug(
                'The GoodReads API was hit %s %s for keyword %s' % (api_hits, plural(api_hits, "time"), searchterm))

            queue.put(resultlist)

        except Exception:
            logger.error('Unhandled exception in GR.find_results: %s' % traceback.format_exc())

    def find_author_id(self, refresh=False):
        author = self.name
        author = formatAuthorName(unaccented(author, only_ascii='_'))
        # googlebooks gives us author names with long form unicode characters
        author = makeUnicode(author)  # ensure it's unicode
        author = unicodedata.normalize('NFC', author)  # normalize to short form
        logger.debug("Searching for author with name: %s" % author)
        URL = '/'.join([lazylibrarian.CONFIG['GR_URL'], 'api/author_url/'])
        try:
            URL += quote(makeUTF8bytes(author)[0]) + '?' + urlencode(self.params)
            if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                logger.debug(URL)
            rootxml, _ = gr_xml_request(URL, useCache=not refresh)
        except Exception as e:
            logger.error("%s finding authorid: %s, %s" % (type(e).__name__, URL, str(e)))
            return {}
        if rootxml is None:
            logger.debug("Error requesting authorid")
            return {}

        resultxml = rootxml.iter('author')

        if resultxml is None:
            logger.warn('No authors found with name: %s' % author)
            return {}

        # In spite of how this looks, goodreads only returns one result, even if there are multiple matches
        # we just have to hope we get the right one. eg search for "James Lovelock" returns "James E. Lovelock"
        # who only has one book listed under googlebooks, the rest are under "James Lovelock"
        # goodreads has all his books under "James E. Lovelock". Can't come up with a good solution yet.
        # For now we'll have to let the user handle this by selecting/adding the author manually
        for res in resultxml:
            authorid = res.attrib.get("id")
            authorname = res.find('name').text
            authorname = formatAuthorName(unaccented(authorname, only_ascii=False))
            match = fuzz.ratio(author, authorname)
            if match >= lazylibrarian.CONFIG['NAME_RATIO']:
                return self.get_author_info(authorid)
            else:
                logger.debug("Fuzz failed: %s [%s][%s]" % (match, author, authorname))
        return {}

    def get_author_info(self, authorid=None):

        URL = '/'.join([lazylibrarian.CONFIG['GR_URL'],
                        'author/show/' + authorid + '.xml?' + urlencode(self.params)])

        try:
            if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                logger.debug(URL)
            rootxml, _ = gr_xml_request(URL)
        except Exception as e:
            logger.error("%s getting author info: %s" % (type(e).__name__, str(e)))
            return {}
        if rootxml is None:
            logger.debug("Error requesting author info")
            return {}

        resultxml = rootxml.find('author')
        if resultxml is None:
            logger.warn('No author found with ID: ' + authorid)
            return {}

        # added authorname to author_dict - this holds the intact name preferred by GR
        # except GR messes up names like "L. E. Modesitt, Jr." where it returns <name>Jr., L. E. Modesitt</name>
        authorname = resultxml[1].text
        if "," in authorname:
            postfix = getList(lazylibrarian.CONFIG['NAME_POSTFIX'])
            words = authorname.split(',')
            if len(words) == 2:
                if words[0].strip().strip('.').lower in postfix:
                    authorname = words[1].strip() + ' ' + words[0].strip()

        logger.debug("[%s] Processing info for authorID: %s" % (authorname, authorid))
        author_dict = {
            'authorid': resultxml[0].text,
            'authorlink': resultxml.find('link').text,
            'authorimg': resultxml.find('image_url').text,
            'authorborn': resultxml.find('born_at').text,
            'authordeath': resultxml.find('died_at').text,
            'about': resultxml.find('about').text,
            'totalbooks': resultxml.find('works_count').text,
            'authorname': ' '.join(authorname.split())  # remove any extra whitespace
        }
        return author_dict

    @staticmethod
    def get_bookdict(book):
        """ Return all the book info we need as a dictionary or default value if no key """
        mydict = {}
        for val, idx, default in [
            ('name', 'title', ''),
            ('id', 'id', ''),
            ('desc', 'description', ''),
            ('pub', 'publisher', ''),
            ('link', 'link', ''),
            ('rate', 'average_rating', 0.0),
            ('pages', 'num_pages', 0),
            ('pub_year', 'publication_year', '0000'),
            ('pub_month', 'publication_month', '0'),
            ('pub_day', 'publication_day', '0'),
            ('workid', 'work/id', ''),
            ('isbn13', 'isbn13', ''),
            ('isbn10', 'isbn', ''),
            ('img', 'image_url', '')
        ]:

            value = default
            res = book.find(idx)
            if res is not None:
                value = res.text
            if value is None:
                value = default
            if idx == 'rate':
                value = check_float(value, 0.0)
            mydict[val] = value

        return mydict

    def get_author_books(self, authorid=None, authorname=None, bookstatus="Skipped", audiostatus='Skipped',
                         entrystatus='Active', refresh=False, reason='gr.get_author_books'):
        # noinspection PyBroadException
        try:
            entryreason = reason
            api_hits = 0
            gr_lang_hits = 0
            lt_lang_hits = 0
            gb_lang_change = 0
            cache_hits = 0
            not_cached = 0
            URL = '/'.join([lazylibrarian.CONFIG['GR_URL'],
                            'author/list/' + authorid + '.xml?' + urlencode(self.params)])

            # Artist is loading
            myDB = database.DBConnection()
            controlValueDict = {"AuthorID": authorid}
            newValueDict = {"Status": "Loading"}
            myDB.upsert("authors", newValueDict, controlValueDict)
            try:
                if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                    logger.debug(URL)
                rootxml, in_cache = gr_xml_request(URL, useCache=not refresh)
            except Exception as e:
                logger.error("%s fetching author books: %s" % (type(e).__name__, str(e)))
                return
            if rootxml is None:
                logger.debug("Error requesting author books")
                return
            if not in_cache:
                api_hits += 1

            resultxml = rootxml.iter('book')

            valid_langs = getList(lazylibrarian.CONFIG['IMP_PREFLANG'])

            removedResults = 0
            duplicates = 0
            ignored = 0
            added_count = 0
            updated_count = 0
            book_ignore_count = 0
            total_count = 0
            locked_count = 0
            loopCount = 0
            cover_count = 0
            isbn_count = 0
            cover_time = 0
            isbn_time = 0
            auth_start = time.time()
            # these are reject reasons we might want to override, so optionally add to database as "ignored"
            ignorable = ['future', 'date', 'isbn', 'word', 'set']
            if lazylibrarian.CONFIG['NO_LANG']:
                ignorable.append('lang')

            if resultxml is None:
                logger.warn('[%s] No books found for author with ID: %s' % (authorname, authorid))
            else:
                logger.debug("[%s] Now processing books with GoodReads API" % authorname)
                authorNameResult = rootxml.find('./author/name').text
                # Goodreads sometimes puts extra whitespace in the author names!
                authorNameResult = ' '.join(authorNameResult.split())
                logger.debug("GoodReads author name [%s]" % authorNameResult)
                loopCount = 1
                threadname = threading.currentThread().name
                while resultxml:
                    if lazylibrarian.STOPTHREADS and threadname == "AUTHORUPDATE":
                        logger.debug("Aborting %s" % threadname)
                        break
                    for book in resultxml:
                        if lazylibrarian.STOPTHREADS and threadname == "AUTHORUPDATE":
                            logger.debug("Aborting %s" % threadname)
                            break
                        total_count += 1
                        rejected = None
                        booksub = ''
                        series = ''
                        seriesNum = ''
                        bookLanguage = "Unknown"
                        find_field = "id"
                        bookisbn = ""
                        isbnhead = ""
                        originalpubdate = ""
                        bookgenre = ''

                        bookdict = self.get_bookdict(book)

                        bookname = bookdict['name']
                        bookid = bookdict['id']
                        bookdesc = bookdict['desc']
                        bookpub = bookdict['pub']
                        booklink = bookdict['link']
                        bookrate = bookdict['rate']
                        bookpages = bookdict['pages']
                        bookimg = bookdict['img']
                        workid = bookdict['workid']
                        isbn13 = bookdict['isbn13']
                        isbn10 = bookdict['isbn10']
                        bookdate = bookdict['pub_year']
                        if check_year(bookdate, past=1800, future=0):
                            mn = check_int(bookdict['pub_month'], 0)
                            dy = check_int(bookdict['pub_day'], 0)
                            if mn and dy:
                                bookdate = "%s-%02d-%02d" % (bookdate, mn, dy)

                        if not bookname:
                            logger.debug('Rejecting bookid %s for %s, no bookname' %
                                         (bookid, authorNameResult))
                            rejected = 'name', 'No bookname'

                        if bookpub:
                            if bookpub.lower() in getList(lazylibrarian.CONFIG['REJECT_PUBLISHER']):
                                logger.warn("Ignoring %s: Publisher %s" % (bookname, bookpub))
                                rejected = 'publisher', bookpub

                        bookname = replace_all(bookname, {':': ' ', '"': '', '\'': ''}).strip()

                        # if not rejected and re.match(r'[^\w-]', bookname):
                        # reject books with bad characters in title
                        # logger.debug("removed result [" + bookname + "] for bad characters")
                        # rejected = 'chars', 'Bad characters in bookname'

                        if not rejected:
                            if not bookimg or 'nocover' in bookimg or 'nophoto' in bookimg:
                                bookimg = 'images/nocover.png'

                            if isbn13:
                                find_field = "isbn13"
                                bookisbn = isbn13
                                isbnhead = bookisbn[3:6]
                            elif isbn10:
                                find_field = "isbn"
                                bookisbn = isbn10
                                isbnhead = bookisbn[0:3]

                            # Try to use shortcut of ISBN identifier codes described here...
                            # http://en.wikipedia.org/wiki/List_of_ISBN_identifier_groups
                            if isbnhead:
                                if find_field == "isbn13" and bookisbn.startswith('979'):
                                    for item in lazylibrarian.isbn_979_dict:
                                        if isbnhead.startswith(item):
                                            bookLanguage = lazylibrarian.isbn_979_dict[item]
                                            break
                                    if bookLanguage != "Unknown":
                                        logger.debug("ISBN979 returned %s for %s" % (bookLanguage, isbnhead))
                                elif (find_field == "isbn") or (find_field == "isbn13" and
                                                                bookisbn.startswith('978')):
                                    for item in lazylibrarian.isbn_978_dict:
                                        if isbnhead.startswith(item):
                                            bookLanguage = lazylibrarian.isbn_978_dict[item]
                                            break
                                    if bookLanguage != "Unknown":
                                        logger.debug("ISBN978 returned %s for %s" % (bookLanguage, isbnhead))

                            if bookLanguage == "Unknown" and isbnhead:
                                # Nothing in the isbn dictionary, try any cached results
                                match = myDB.match('SELECT lang FROM languages where isbn=?', (isbnhead,))
                                if match:
                                    bookLanguage = match['lang']
                                    cache_hits += 1
                                    logger.debug("Found cached language [%s] for %s [%s]" %
                                                 (bookLanguage, find_field, isbnhead))
                                else:
                                    bookLanguage = thingLang(bookisbn)
                                    lt_lang_hits += 1
                                    if bookLanguage:
                                        myDB.action('insert into languages values (?, ?)', (isbnhead, bookLanguage))

                            if not bookLanguage or bookLanguage == "Unknown":
                                # still  no earlier match, we'll have to search the goodreads api
                                try:
                                    if book.find(find_field).text:
                                        BOOK_URL = '/'.join([lazylibrarian.CONFIG['GR_URL'], 'book/show?id=' +
                                                             book.find(find_field).text + '&' +
                                                             urlencode(self.params)])
                                        logger.debug("Book URL: " + BOOK_URL)
                                        bookLanguage = ""
                                        try:
                                            BOOK_rootxml, in_cache = gr_xml_request(BOOK_URL)
                                            if BOOK_rootxml is None:
                                                logger.debug('Error requesting book page')
                                            else:
                                                try:
                                                    bookLanguage = BOOK_rootxml.find('./book/language_code').text
                                                except Exception as e:
                                                    logger.error("%s finding language_code in book xml: %s" %
                                                                 (type(e).__name__, str(e)))
                                                # noinspection PyBroadException
                                                try:
                                                    res = BOOK_rootxml.find('./book/isbn').text
                                                    isbnhead = res[0:3]
                                                except Exception:
                                                    # noinspection PyBroadException
                                                    try:
                                                        res = BOOK_rootxml.find('./book/isbn13').text
                                                        isbnhead = res[3:6]
                                                    except Exception:
                                                        isbnhead = ''
                                                # if bookLanguage and not isbnhead:
                                                #     print(BOOK_URL)

                                                # might as well get the original publication date from here
                                                # noinspection PyBroadException
                                                try:
                                                    bookdate = BOOK_rootxml.find(
                                                        './book/work/original_publication_year').text
                                                    if check_year(bookdate, past=1800, future=0):
                                                        try:
                                                            mn = check_int(BOOK_rootxml.find(
                                                                './book/work/original_publication_month').text, 0)
                                                            dy = check_int(BOOK_rootxml.find(
                                                                './book/work/original_publication_day').text, 0)
                                                            if mn and dy:
                                                                bookdate = "%s-%02d-%02d" % (bookdate, mn, dy)
                                                        except (KeyError, AttributeError):
                                                            logger.debug("No extended date info")
                                                            pass
                                                except Exception:
                                                    pass

                                        except Exception as e:
                                            logger.error("%s getting book xml: %s" % (type(e).__name__, str(e)))

                                        if not in_cache:
                                            gr_lang_hits += 1
                                        if not bookLanguage:
                                            bookLanguage = "Unknown"
                                        elif isbnhead:
                                            # if GR didn't give an isbn we can't cache it
                                            # just use language for this book
                                            controlValueDict = {"isbn": isbnhead}
                                            newValueDict = {"lang": bookLanguage}
                                            myDB.upsert("languages", newValueDict, controlValueDict)
                                            logger.debug("GoodReads reports language [%s] for %s" %
                                                         (bookLanguage, isbnhead))
                                        else:
                                            not_cached += 1

                                        logger.debug("GR language: " + bookLanguage)
                                    else:
                                        logger.debug("No %s provided for [%s]" % (find_field, bookname))
                                        # continue

                                except Exception as e:
                                    logger.error("Goodreads language search failed: %s %s" %
                                                 (type(e).__name__, str(e)))

                            if not isbnhead and lazylibrarian.CONFIG['ISBN_LOOKUP']:
                                # try lookup by name
                                if bookname:
                                    try:
                                        isbn_count += 1
                                        start = time.time()
                                        res = isbn_from_words(unaccented(bookname, only_ascii=False) + ' ' +
                                                              unaccented(authorNameResult, only_ascii=False))
                                        isbn_time += (time.time() - start)
                                    except Exception as e:
                                        res = None
                                        logger.warn("Error from isbn: %s" % e)
                                    if res:
                                        logger.debug("isbn found %s for %s" % (res, bookid))
                                        bookisbn = res
                                        if len(res) == 13:
                                            isbnhead = res[3:6]
                                        else:
                                            isbnhead = res[0:3]

                            if not isbnhead and lazylibrarian.CONFIG['NO_ISBN']:
                                rejected = 'isbn', 'No ISBN'
                                logger.debug('Rejecting %s, %s' % (bookname, rejected[1]))

                            if "All" not in valid_langs:  # do we care about language
                                if bookLanguage not in valid_langs:
                                    rejected = 'lang', 'Invalid language [%s]' % bookLanguage
                                    logger.debug('Rejecting %s, %s' % (bookname, rejected[1]))
                                    ignored += 1

                        if not rejected:
                            dic = {'.': ' ', '-': ' ', '/': ' ', '+': ' ', '_': ' ', '(': '', ')': '',
                                   '[': ' ', ']': ' ', '#': '# ', ':': ' ', ';': ' '}
                            name = replace_all(bookname, dic).strip()
                            name = name.lower()
                            # remove extra spaces if they're in a row
                            name = " ".join(name.split())
                            namewords = name.split(' ')
                            badwords = getList(lazylibrarian.CONFIG['REJECT_WORDS'], ',')
                            for word in badwords:
                                if (' ' in word and word in name) or word in namewords:
                                    rejected = 'word', 'Contains [%s]' % word
                                    logger.debug('Rejecting %s, %s' % (bookname, rejected[1]))
                                    break

                        if not rejected:
                            bookname = unaccented(bookname, only_ascii=False)
                            if lazylibrarian.CONFIG['NO_SETS']:
                                # allow date ranges eg 1981-95
                                m = re.search(r'(\d+)-(\d+)', bookname)
                                if m:
                                    if check_year(m.group(1), past=1800, future=0):
                                        logger.debug("Allow %s, looks like a date range" % bookname)
                                    else:
                                        rejected = 'set', 'Set or Part %s' % m.group(0)
                                        logger.debug('Rejected %s, %s' % (bookname, rejected[1]))
                                elif re.search(r'\d+ of \d+', bookname) or \
                                        re.search(r'\d+/\d+', bookname):
                                    rejected = 'set', 'Set or Part'
                                    logger.debug('Rejected %s, %s' % (bookname, rejected[1]))

                        if not rejected:
                            bookname, booksub = split_title(authorNameResult, bookname)
                            dic = {':': '.', '"': ''}  # do we need to strip apostrophes , '\'': ''}
                            bookname = replace_all(bookname, dic)
                            bookname = bookname.strip()
                            booksub = replace_all(booksub, dic)
                            booksub = booksub.strip()
                            if booksub:
                                seriesdetails = booksub
                            else:
                                seriesdetails = bookname

                            series, seriesNum = bookSeries(seriesdetails)

                            # 1. The author/list page only contains one author per book even if the book/show page
                            #    and html show multiple authors
                            # 2. The author/list page doesn't always include the publication date even if the
                            #    book/show page and html include it
                            # 3. The author/list page gives the publication date of the "best book" edition
                            #    and does not include the original publication date, though the book/show page
                            #    and html often show it
                            # We can't call book/show for every book because of api limits, and we can't scrape
                            # the html as it breaks goodreads terms of service
                            authors = book.find('authors')
                            anames = authors.iter('author')
                            amatch = False
                            alist = ''
                            role = ''
                            for aname in anames:
                                aid = aname.find('id').text
                                anm = aname.find('name').text
                                role = aname.find('role').text
                                if alist:
                                    alist += ', '
                                alist += anm
                                if aid == authorid or anm == authorNameResult:
                                    if aid != authorid:
                                        logger.warn("Author %s has different authorid %s:%s" % (anm, aid, authorid))
                                    if role is None or 'author' in role.lower() or \
                                            'writer' in role.lower() or \
                                            'creator' in role.lower() or \
                                            'pseudonym' in role.lower() or \
                                            'pen name' in role.lower():
                                        amatch = True
                                    else:
                                        logger.debug('Ignoring %s for %s, role is %s' % (anm, bookname, role))
                            if not amatch:
                                rejected = 'author', 'Wrong Author (got %s,%s)' % (alist, role)
                                logger.debug('Rejecting %s for %s, %s' %
                                             (bookname, authorNameResult, rejected[1]))

                        cmd = 'SELECT AuthorName,BookName,AudioStatus,books.Status,ScanResult '
                        cmd += 'FROM books,authors WHERE authors.AuthorID = books.AuthorID AND BookID=?'
                        match = myDB.match(cmd, (bookid,))
                        rejectable = None
                        if match:
                            # we have a book with this bookid already
                            if authorNameResult != match['AuthorName']:
                                rejected = 'author', 'Different author for this bookid [%s][%s]' % (
                                            authorNameResult, match['AuthorName'])
                                logger.debug('Rejecting bookid %s, %s' % (bookid, rejected[1]))
                            elif bookname != match['BookName']:
                                # same bookid and author, assume goodreads fixed the title, use the new title
                                myDB.action("UPDATE books SET BookName=? WHERE BookID=?", (bookname, bookid))
                                logger.warn('Updated bookname [%s] to [%s]' % (match['BookName'], bookname))

                            msg = 'Bookid %s for [%s][%s] is in database marked %s' % (
                                   bookid, authorNameResult, bookname, match['Status'])
                            if lazylibrarian.SHOW_AUDIO:
                                msg += ",%s" % match['AudioStatus']
                            msg += " %s" % match['ScanResult']
                            logger.debug(msg)

                            # Make sure we don't reject books we have already got or want
                            if match['Status'] not in ['Ignored', 'Skipped']:
                                rejectable = "Status: %s" % match['Status']
                            elif match['AudioStatus'] not in ['Ignored', 'Skipped']:
                                rejectable = "AudioStatus: %s" % match['AudioStatus']

                        if not rejected:
                            cmd = 'SELECT BookID FROM books,authors WHERE books.AuthorID = authors.AuthorID'
                            cmd += ' and BookName=? COLLATE NOCASE and AuthorName=? COLLATE NOCASE'
                            match = myDB.match(cmd, (bookname, authorNameResult))

                            if match and match['BookID'] != bookid:
                                # we have a different bookid for this author/title already
                                if rejectable is None:
                                    duplicates += 1
                                    rejected = 'bookid', 'Got %s under bookid %s' % (bookid, match['BookID'])
                                    logger.debug('Rejecting bookid %s for [%s][%s] already got %s' %
                                                 (bookid, authorNameResult, bookname, match['BookID']))
                                else:
                                    logger.debug("Not rejecting duplicate title %s (%s/%s) as %s" %
                                                 (bookname, bookid, match['BookID'], rejectable))

                        if rejected and rejected[0] not in ignorable:
                            removedResults += 1
                        if not rejected or (rejected and rejected[0] in ignorable and
                                            lazylibrarian.CONFIG['IMP_IGNORE']):
                            cmd = 'SELECT Status,AudioStatus,BookFile,AudioFile,Manual,BookAdded,BookName,'
                            cmd += 'OriginalPubDate,BookDesc,BookGenre,ScanResult FROM books WHERE BookID=?'
                            existing = myDB.match(cmd, (bookid,))
                            if existing:
                                book_status = existing['Status']
                                audio_status = existing['AudioStatus']
                                bookdesc = existing['BookDesc']
                                bookgenre = existing['BookGenre']
                                if lazylibrarian.CONFIG['FOUND_STATUS'] == 'Open':
                                    if book_status == 'Have' and existing['BookFile']:
                                        book_status = 'Open'
                                    if audio_status == 'Have' and existing['AudioFile']:
                                        audio_status = 'Open'
                                locked = existing['Manual']
                                added = existing['BookAdded']
                                if locked is None:
                                    locked = False
                                elif locked.isdigit():
                                    locked = bool(int(locked))
                                if not originalpubdate:
                                    originalpubdate = existing['OriginalPubDate']
                            else:
                                book_status = bookstatus  # new_book status, or new_author status
                                audio_status = audiostatus
                                added = today()
                                locked = False

                            if not originalpubdate or len(originalpubdate) < 5:
                                # already set with language code or existing book?
                                newdate, in_cache = getBookPubdate(bookid)
                                if not originalpubdate:
                                    originalpubdate = newdate
                                elif originalpubdate < newdate:  # more detailed date
                                    originalpubdate = newdate
                                    logger.debug("Extended date info found: %s" % newdate)
                                if not in_cache:
                                    api_hits += 1

                            if originalpubdate:
                                bookdate = originalpubdate

                            if not rejected and lazylibrarian.CONFIG['NO_FUTURE']:
                                if bookdate > today()[:len(bookdate)]:
                                    if rejectable is None:
                                        rejected = 'future', 'Future publication date [%s]' % bookdate
                                        logger.debug('Rejecting %s, %s' % (bookname, rejected[1]))
                                    else:
                                        logger.debug("Not rejecting %s (future pub date %s) as %s" %
                                                     (bookname, bookdate, rejectable))

                            if not rejected and lazylibrarian.CONFIG['NO_PUBDATE']:
                                if not bookdate or bookdate == '0000':
                                    if rejectable is None:
                                        rejected = 'date', 'No publication date'
                                        logger.debug('Rejecting %s, %s' % (bookname, rejected[1]))
                                    else:
                                        logger.debug("Not rejecting %s (no pub date) as %s" %
                                                     (bookname, rejectable))

                            if rejected:
                                if rejected[0] in ignorable:
                                    book_status = 'Ignored'
                                    audio_status = 'Ignored'
                                    book_ignore_count += 1
                                    reason = "Ignored: %s" % rejected[1]
                                else:
                                    reason = "Rejected: %s" % rejected[1]
                            else:
                                if 'authorUpdate' in entryreason:
                                    reason = 'Author: %s' % authorNameResult
                                else:
                                    reason = entryreason

                            # Leave alone if locked
                            if locked:
                                locked_count += 1
                            else:
                                if not bookgenre:
                                    genres, _ = get_gr_genres(bookid)
                                    if genres:
                                        bookgenre = ', '.join(genres)
                                infodict = get_gb_info(isbn=bookisbn, author=authorNameResult,
                                                       title=bookname, expire=False)
                                if infodict:  # None if api blocked
                                    gbupdate = []
                                    if not bookdesc and infodict['desc']:
                                        bookdesc = infodict['desc']
                                        gbupdate.append('Description')
                                    if not bookdate or bookdate == '0000' or len(infodict['date']) > len(bookdate):
                                        bookdate = infodict['date']
                                        gbupdate.append('Publication Date')
                                    if infodict['rate'] and not bookrate:
                                        bookrate = infodict['rate']
                                        gbupdate.append('Rating')
                                    if infodict['pub'] and not bookpub:
                                        bookpub = infodict['pub']
                                        gbupdate.append('Publisher')
                                    if infodict['pages'] and not bookpages:
                                        bookpages = infodict['pages']
                                        gbupdate.append('Pages')
                                    if not bookgenre and infodict['genre']:
                                        bookgenre = genreFilter(infodict['genre'])
                                        gbupdate.append('Genres')
                                    if gbupdate:
                                        logger.debug("Updated %s from googlebooks" % ', '.join(gbupdate))

                                threadname = threading.currentThread().getName()
                                reason = "[%s] %s" % (threadname, reason)
                                controlValueDict = {"BookID": bookid}
                                newValueDict = {
                                    "AuthorID": authorid,
                                    "BookName": bookname,
                                    "BookSub": booksub,
                                    "BookDesc": bookdesc,
                                    "BookIsbn": bookisbn,
                                    "BookPub": bookpub,
                                    "BookGenre": bookgenre,
                                    "BookImg": bookimg,
                                    "BookLink": booklink,
                                    "BookRate": bookrate,
                                    "BookPages": bookpages,
                                    "BookDate": bookdate,
                                    "BookLang": bookLanguage,
                                    "Status": book_status,
                                    "AudioStatus": audio_status,
                                    "BookAdded": added,
                                    "WorkID": workid,
                                    "ScanResult": reason,
                                    "OriginalPubDate": originalpubdate
                                }
                                myDB.upsert("books", newValueDict, controlValueDict)

                                setGenres(getList(bookgenre, ','), bookid)

                                updateValueDict = {}
                                # need to run getWorkSeries AFTER adding to book table (foreign key constraint)
                                serieslist = []
                                if series:
                                    serieslist = [('', seriesNum, cleanName(series, '&/'))]
                                if lazylibrarian.CONFIG['ADD_SERIES'] and "Ignored:" not in reason:
                                    newserieslist = getWorkSeries(workid, reason=reason)
                                    if newserieslist:
                                        serieslist = newserieslist
                                        logger.debug('Updated series: %s [%s]' % (bookid, serieslist))
                                    _api_hits, pubdate = setSeries(serieslist, bookid, authorid, workid, reason=reason)
                                    api_hits += _api_hits
                                    if pubdate and pubdate > originalpubdate:  # more detailed
                                        updateValueDict["OriginalPubDate"] = pubdate

                                if not rejected:
                                    if existing and existing['ScanResult'] and \
                                            ' publication date' in existing['ScanResult'] and \
                                            bookdate and bookdate != '0000' and \
                                            bookdate <= today()[:len(bookdate)]:
                                        # was rejected on previous scan but bookdate has become valid
                                        logger.debug("valid bookdate [%s] previous scanresult [%s]" %
                                                     (bookdate, existing['ScanResult']))
                                        updateValueDict["ScanResult"] = "bookdate %s is now valid" % bookdate
                                    elif not existing:
                                        updateValueDict["ScanResult"] = reason

                                    if "ScanResult" in updateValueDict:
                                        if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                                            logger.debug("entry status %s %s,%s" % (entrystatus,
                                                                                    bookstatus,
                                                                                    audiostatus))
                                        book_status, audio_status = getStatus(bookid, serieslist, bookstatus,
                                                                              audiostatus, entrystatus)
                                        if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                                            logger.debug("status is now %s,%s" % (book_status,
                                                                                  audio_status))
                                        updateValueDict["Status"] = book_status
                                        updateValueDict["AudioStatus"] = audio_status

                                    if 'nocover' in bookimg or 'nophoto' in bookimg:
                                        # try to get a cover from another source
                                        start = time.time()
                                        workcover, source = getBookCover(bookid)
                                        if source != 'cache':
                                            cover_count += 1
                                            cover_time += (time.time() - start)

                                        if workcover:
                                            logger.debug('Updated cover for %s using %s' % (bookname, source))
                                            updateValueDict["BookImg"] = workcover

                                    elif bookimg and bookimg.startswith('http'):
                                        start = time.time()
                                        link, success, was_already_cached = cache_img("book", bookid, bookimg)
                                        if not was_already_cached:
                                            cover_count += 1
                                            cover_time += (time.time() - start)
                                        if success:
                                            updateValueDict["BookImg"] = link
                                        else:
                                            logger.debug('Failed to cache image for %s' % bookimg)

                                    worklink = getWorkPage(bookid)
                                    if worklink:
                                        updateValueDict["WorkPage"] = worklink

                                if updateValueDict:
                                    myDB.upsert("books", updateValueDict, controlValueDict)

                                if not existing:
                                    typ = 'Added'
                                    added_count += 1
                                else:
                                    typ = 'Updated'
                                    updated_count += 1
                                msg = "[%s] %s book: %s [%s] status %s" % (authorname, typ, bookname,
                                                                           bookLanguage, book_status)
                                if lazylibrarian.SHOW_AUDIO:
                                    msg += " audio %s" % audio_status
                                logger.debug(msg)
                    loopCount += 1
                    if 0 < lazylibrarian.CONFIG['MAX_BOOKPAGES'] < loopCount:
                        resultxml = None
                    else:
                        URL = '/'.join([lazylibrarian.CONFIG['GR_URL'], 'author/list/' + authorid + '.xml?' +
                                        urlencode(self.params) + '&page=' + str(loopCount)])
                        resultxml = None
                        try:
                            if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                                logger.debug(URL)
                            rootxml, in_cache = gr_xml_request(URL, useCache=not refresh)
                            if rootxml is None:
                                logger.debug('Error requesting next page of results')
                            else:
                                resultxml = rootxml.iter('book')
                                if not in_cache:
                                    api_hits += 1
                        except Exception as e:
                            resultxml = None
                            logger.error("%s finding next page of results: %s" % (type(e).__name__, str(e)))

                    if resultxml:
                        if all(False for _ in resultxml):  # returns True if iterator is empty
                            resultxml = None

            self.verify_ids(authorid)
            deleteEmptySeries()
            cmd = 'SELECT BookName, BookLink, BookDate, BookImg, BookID from books WHERE AuthorID=?'
            cmd += ' AND Status != "Ignored" order by BookDate DESC'
            lastbook = myDB.match(cmd, (authorid,))
            if lastbook:
                lastbookname = lastbook['BookName']
                lastbooklink = lastbook['BookLink']
                lastbookdate = lastbook['BookDate']
                lastbookid = lastbook['BookID']
                lastbookimg = lastbook['BookImg']
            else:
                lastbookname = ""
                lastbooklink = ""
                lastbookdate = ""
                lastbookid = ""
                lastbookimg = ""

            controlValueDict = {"AuthorID": authorid}
            newValueDict = {
                "Status": entrystatus,
                "LastBook": lastbookname,
                "LastLink": lastbooklink,
                "LastDate": lastbookdate,
                "LastBookID": lastbookid,
                "LastBookImg": lastbookimg
            }
            myDB.upsert("authors", newValueDict, controlValueDict)

            resultcount = added_count + updated_count
            loopCount -= 1
            logger.debug("Found %s %s in %s %s" % (total_count, plural(total_count, "result"),
                                                   loopCount, plural(loopCount, "page")))
            logger.debug("Found %s locked %s" % (locked_count, plural(locked_count, "book")))
            logger.debug("Removed %s unwanted language %s" % (ignored, plural(ignored, "result")))
            logger.debug("Removed %s incorrect/incomplete %s" % (removedResults, plural(removedResults, "result")))
            logger.debug("Removed %s duplicate %s" % (duplicates, plural(duplicates, "result")))
            logger.debug("Ignored %s %s" % (book_ignore_count, plural(book_ignore_count, "book")))
            logger.debug("Imported/Updated %s %s in %d secs using %s api %s" %
                         (resultcount, plural(resultcount, "book"), int(time.time() - auth_start),
                          api_hits, plural(api_hits, "hit")))
            if cover_count:
                logger.debug("Fetched %s %s in %.2f sec" % (cover_count, plural(cover_count, "cover"), cover_time))
            if isbn_count:
                logger.debug("Fetched %s ISBN in %.2f sec" % (isbn_count, isbn_time))

            controlValueDict = {"authorname": authorname.replace('"', '""')}
            newValueDict = {
                            "GR_book_hits": api_hits,
                            "GR_lang_hits": gr_lang_hits,
                            "LT_lang_hits": lt_lang_hits,
                            "GB_lang_change": gb_lang_change,
                            "cache_hits": cache_hits,
                            "bad_lang": ignored,
                            "bad_char": removedResults,
                            "uncached": not_cached,
                            "duplicates": duplicates
                            }
            myDB.upsert("stats", newValueDict, controlValueDict)

            if refresh:
                logger.info("[%s] Book processing complete: Added %s %s / Updated %s %s" %
                            (authorname, added_count, plural(added_count, "book"),
                             updated_count, plural(updated_count, "book")))
            else:
                logger.info("[%s] Book processing complete: Added %s %s to the database" %
                            (authorname, added_count, plural(added_count, "book")))

        except Exception:
            logger.error('Unhandled exception in GR.get_author_books: %s' % traceback.format_exc())

    def verify_ids(self, authorid):
        """ GoodReads occasionally consolidates bookids/workids and renumbers so check if changed... """
        myDB = database.DBConnection()
        cmd = "select BookID,BookName from books WHERE AuthorID=?"
        books = myDB.select(cmd, (authorid,))
        counter = 0
        logger.debug('Checking BookID/WorkID for %s %s' % (len(books), plural(len(books), "book")))
        page = ''
        pages = []
        for book in books:
            bookid = book['BookID']
            if not bookid:
                logger.warn("No bookid for %s" % book['BookName'])
            else:
                if page:
                    page = page + ','
                page = page + bookid
                counter += 1
                if counter == 50:
                    counter = 0
                    pages.append(page)
                    page = ''
        if page:
            pages.append(page)

        found = 0
        differ = 0
        notfound = []
        pagecount = 0
        for page in pages:
            pagecount += 1
            URL = '/'.join([lazylibrarian.CONFIG['GR_URL'], 'book/id_to_work_id/' + page + '?' +
                            urlencode(self.params)])
            try:
                if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                    logger.debug(URL)
                rootxml, _ = gr_xml_request(URL, useCache=False)
                if rootxml is None:
                    logger.debug("Error requesting id_to_work_id page")
                else:
                    resultxml = rootxml.find('work-ids')
                    if len(resultxml):
                        ids = resultxml.iter('item')
                        books = getList(page)
                        cnt = 0
                        for item in ids:
                            workid = item.text
                            if not workid:
                                notfound.append(books[cnt])
                                logger.debug("No workid returned for %s" % books[cnt])
                            else:
                                found += 1
                                res = myDB.match("SELECT WorkID from books WHERE bookid=?", (books[cnt],))
                                if res:
                                    if res['WorkID'] != workid:
                                        differ += 1
                                        logger.debug("Updating workid for %s from [%s] to [%s]" % (
                                                     books[cnt], res['WorkID'], workid))
                                        controlValueDict = {"BookID": books[cnt]}
                                        newValueDict = {"WorkID": workid}
                                        myDB.upsert("books", newValueDict, controlValueDict)
                            cnt += 1

            except Exception as e:
                logger.error("%s parsing id_to_work_id page: %s" % (type(e).__name__, str(e)))
        logger.debug("BookID/WorkID Found %d, Differ %d, Missing %d" % (found, differ, len(notfound)))

        cnt = 0
        for bookid in notfound:
            res = myDB.match("SELECT BookName,Status,AudioStatus from books WHERE bookid=?", (bookid,))
            if res:
                if lazylibrarian.CONFIG['FULL_SCAN']:
                    if res['Status'] in ['Wanted', 'Open', 'Have']:
                        logger.warn("Keeping unknown goodreads bookid %s: %s, Status is %s" %
                                    (bookid, res['BookName'], res['Status']))
                    elif res['AudioStatus'] in ['Wanted', 'Open', 'Have']:
                        logger.warn("Keeping unknown goodreads bookid %s: %s, AudioStatus is %s" %
                                    (bookid, res['BookName'], res['Status']))
                    else:
                        logger.debug("Deleting unknown goodreads bookid %s: %s" % (bookid, res['BookName']))
                        myDB.action("DELETE from books WHERE BookID=?", (bookid,))
                        cnt += 1
                else:
                    logger.warn("Unknown goodreads bookid %s: %s" % (bookid, res['BookName']))
        if cnt:
            logger.warn("Deleted %s %s with unknown goodreads bookid" % (cnt, plural(cnt, 'entry')))

        # Check for any duplicate titles for this author in the library
        cmd = "select count('bookname'),bookname from books where authorid=? "
        cmd += "group by bookname having ( count(bookname) > 1 )"
        res = myDB.select(cmd, (authorid,))
        dupes = len(res)
        if dupes:
            for item in res:
                cmd = "select BookID,Status,AudioStatus from books where bookname=? and authorid=?"
                dupe_books = myDB.select(cmd, (item['bookname'], authorid))
                cnt = len(dupe_books)
                for dupe in dupe_books:
                    cnt -= 1
                    if dupe['Status'] not in ['Ignored', 'Skipped'] \
                            or dupe['AudioStatus'] not in ['Ignored', 'Skipped']:
                        # this one is important (owned/wanted/snatched)
                        logger.debug("Keeping bookid %s (%s/%s)" %
                                     (dupe['BookID'], dupe['Status'], dupe['AudioStatus']))
                    elif cnt:
                        logger.debug("Removing bookid %s (%s/%s)" %
                                     (dupe['BookID'], dupe['Status'], dupe['AudioStatus']))
                    else:
                        logger.debug("Not removing bookid %s (%s/%s) last entry for %s" %
                                     (dupe['BookID'], dupe['Status'], dupe['AudioStatus'], item['bookname']))

        # Warn about any remaining unignored dupes
        cmd = "select count('bookname'),bookname from books where authorid=? and "
        cmd += "( Status != 'Ignored' or AudioStatus != 'Ignored' ) group by bookname having ( count(bookname) > 1 )"
        res = myDB.select(cmd, (authorid,))
        dupes = len(res)
        if dupes:
            author = myDB.match("SELECT AuthorName from authors where AuthorID=?", (authorid,))
            logger.warn("There %s %s duplicate %s for %s" % (plural(dupes, 'is'), dupes, plural(dupes, 'title'),
                                                             author['AuthorName']))
            for item in res:
                logger.debug("%02d: %s" % (item[0], item[1]))

    def find_book(self, bookid=None, bookstatus=None, audiostatus=None, reason='gr.find_book'):
        logger.debug("bookstatus=%s, audiostatus=%s" % (bookstatus, audiostatus))
        myDB = database.DBConnection()
        URL = '/'.join([lazylibrarian.CONFIG['GR_URL'], 'book/show/' + bookid + '?' + urlencode(self.params)])
        try:
            if lazylibrarian.LOGLEVEL & lazylibrarian.log_searching:
                logger.debug(URL)
            rootxml, _ = gr_xml_request(URL)
            if rootxml is None:
                logger.debug("Error requesting book")
                return
        except Exception as e:
            logger.error("%s finding book: %s" % (type(e).__name__, str(e)))
            return

        if not bookstatus:
            bookstatus = lazylibrarian.CONFIG['NEWBOOK_STATUS']
            logger.debug("No bookstatus passed, using default %s" % bookstatus)
        if not audiostatus:
            audiostatus = lazylibrarian.CONFIG['NEWAUDIO_STATUS']
            logger.debug("No audiostatus passed, using default %s" % audiostatus)
        logger.debug("bookstatus=%s, audiostatus=%s" % (bookstatus, audiostatus))
        bookLanguage = rootxml.find('./book/language_code').text
        bookname = rootxml.find('./book/title').text

        if not bookLanguage:
            bookLanguage = "Unknown"
        #
        # user has said they want this book, don't block for unwanted language etc
        # Ignore book if adding as part of a series, else just warn and include it
        #
        valid_langs = getList(lazylibrarian.CONFIG['IMP_PREFLANG'])
        if bookLanguage not in valid_langs and 'All' not in valid_langs:
            msg = 'Book %s Language [%s] does not match preference' % (bookname, bookLanguage)
            logger.warn(msg)
            if reason.startswith("Series:"):
                return

        if rootxml.find('./book/work/original_publication_year').text is None:
            originalpubdate = ''
            if rootxml.find('./book/publication_year').text is None:
                bookdate = "0000"
            else:
                bookdate = rootxml.find('./book/publication_year').text
                if check_year(bookdate, past=1800, future=0):
                    try:
                        mn = check_int(rootxml.find('./book/publication_month').text, 0)
                        dy = check_int(rootxml.find('./book/publication_day').text, 0)
                        if mn and dy:
                            bookdate = "%s-%02d-%02d" % (bookdate, mn, dy)
                    except (KeyError, AttributeError):
                        pass
        else:
            originalpubdate = rootxml.find('./book/work/original_publication_year').text
            if check_year(originalpubdate, past=1800, future=0):
                try:
                    mn = check_int(rootxml.find('./book/work/original_publication_month').text, 0)
                    dy = check_int(rootxml.find('./book/work/original_publication_day').text, 0)
                    if mn and dy:
                        originalpubdate = "%s-%02d-%02d" % (originalpubdate, mn, dy)
                except (KeyError, AttributeError):
                    pass
            bookdate = originalpubdate

        if lazylibrarian.CONFIG['NO_PUBDATE']:
            if not bookdate or bookdate == '0000':
                msg = 'Book %s Publication date [%s] does not match preference' % (bookname, bookdate)
                logger.warn(msg)
                if reason.startswith("Series:"):
                    return

        if lazylibrarian.CONFIG['NO_FUTURE']:
            # may have yyyy or yyyy-mm-dd
            if bookdate > today()[:len(bookdate)]:
                msg = 'Book %s Future publication date [%s] does not match preference' % (bookname, bookdate)
                logger.warn(msg)
                if reason.startswith("Series:"):
                    return

        if lazylibrarian.CONFIG['NO_SETS']:
            if re.search(r'\d+ of \d+', bookname) or re.search(r'\d+/\d+', bookname):
                msg = 'Book %s Set or Part' % bookname
                logger.warn(msg)
                if reason.startswith("Series:"):
                    return

            # allow date ranges eg 1981-95
            m = re.search(r'(\d+)-(\d+)', bookname)
            if m:
                if check_year(m.group(1), past=1800, future=0):
                    msg = "Allow %s, looks like a date range" % m.group(1)
                    logger.debug(msg)
                else:
                    msg = 'Set or Part %s' % bookname
                    logger.warn(msg)
                    if reason.startswith("Series:"):
                        return
        try:
            bookimg = rootxml.find('./book/img_url').text
            if not bookimg or 'nocover' in bookimg or 'nophoto' in bookimg:
                bookimg = 'images/nocover.png'
        except (KeyError, AttributeError):
            bookimg = 'images/nocover.png'

        authorname = rootxml.find('./book/authors/author/name').text
        authorid = rootxml.find('./book/authors/author/id').text
        bookdesc = rootxml.find('./book/description').text
        bookisbn = rootxml.find('./book/isbn13').text
        if not bookisbn:
            bookisbn = rootxml.find('./book/isbn').text
        bookpub = rootxml.find('./book/publisher').text
        booklink = rootxml.find('./book/link').text
        bookrate = check_float(rootxml.find('./book/average_rating').text, 0)
        bookpages = rootxml.find('.book/num_pages').text
        workid = rootxml.find('.book/work/id').text

        match = myDB.match('SELECT AuthorName from authors WHERE AuthorID=?', (authorid,))
        if match:
            author = {'authorid': authorid, 'authorname': match['AuthorName']}
        else:
            match = myDB.match('SELECT AuthorID from authors WHERE AuthorName=?', (authorname,))
            if match:
                logger.debug('%s: Changing authorid from %s to %s' %
                             (authorname, authorid, match['AuthorID']))
                author = {'authorid': match['AuthorID'], 'authorname': authorname}
            else:
                GR = GoodReads(authorname)
                author = GR.find_author_id()
        if author:
            AuthorID = author['authorid']
            match = myDB.match('SELECT * from authors WHERE AuthorID=?', (AuthorID,))
            if not match:
                # no author but request to add book, add author with newauthor status
                # User hit "add book" button from a search, or a wishlist import, or api call
                newauthor_status = 'Active'
                if lazylibrarian.CONFIG['NEWAUTHOR_STATUS'] in ['Skipped', 'Ignored']:
                    newauthor_status = 'Paused'
                # also pause author if adding as a series contributor/wishlist/grsync
                if reason.startswith("Series:") or "grsync" in reason or "wishlist" in reason:
                    newauthor_status = 'Paused'
                controlValueDict = {"AuthorID": AuthorID}
                newValueDict = {
                    "AuthorName": author['authorname'],
                    "AuthorImg": author['authorimg'],
                    "AuthorLink": author['authorlink'],
                    "AuthorBorn": author['authorborn'],
                    "AuthorDeath": author['authordeath'],
                    "DateAdded": today(),
                    "Updated": int(time.time()),
                    "Status": newauthor_status,
                    "Reason": reason
                }
                logger.debug("Adding author %s %s, %s" % (AuthorID, author['authorname'], newauthor_status))
                # cmd = 'insert into authors (AuthorID, AuthorName, AuthorImg, AuthorLink, AuthorBorn,'
                # cmd += ' AuthorDeath, DateAdded, Updated, Status, Reason) values (?,?,?,?,?,?,?,?,?,?)'
                # myDB.action(cmd, (AuthorID, author['authorname'], author['authorimg'], author['authorlink'],
                #                   author['authorborn'], author['authordeath'], today(), int(time.time()),
                #                   newauthor_status, reason))

                myDB.upsert("authors", newValueDict, controlValueDict)
                myDB.commit()  # shouldn't really be necessary as context manager commits?
                authorname = author['authorname']
                if lazylibrarian.CONFIG['NEWAUTHOR_BOOKS'] and newauthor_status != 'Paused':
                    self.get_author_books(AuthorID, entrystatus=lazylibrarian.CONFIG['NEWAUTHOR_STATUS'],
                                          reason=reason)
        else:
            logger.warn("No AuthorID for %s, unable to add book %s" % (authorname, bookname))
            return

        # bookname = unaccented(bookname, only_ascii=False)
        bookname, booksub = split_title(authorname, bookname)
        dic = {':': '.', '"': ''}
        bookname = replace_all(bookname, dic).strip()
        booksub = replace_all(booksub, dic).strip()
        if booksub:
            series, seriesNum = bookSeries(booksub)
        else:
            series, seriesNum = bookSeries(bookname)

        if not bookisbn:
            try:
                res = isbn_from_words(bookname + ' ' + unaccented(authorname, only_ascii=False))
            except Exception as e:
                res = None
                logger.warn("Error from isbn: %s" % e)
            if res:
                logger.debug("isbn found %s for %s" % (res, bookname))
                bookisbn = res

        bookgenre = ''
        genres, _ = get_gr_genres(bookid)
        if genres:
            bookgenre = ', '.join(genres)
        if not bookdesc:
            infodict = get_gb_info(isbn=bookisbn, author=authorname, title=bookname, expire=False)
            if infodict is not None:  # None if api blocked
                if infodict and infodict['desc']:
                    bookdesc = infodict['desc']
                else:
                    bookdesc = 'No Description'
                if not bookgenre:
                    if infodict and infodict['genre']:
                        bookgenre = genreFilter(infodict['genre'])
                    else:
                        bookgenre = 'Unknown'

        threadname = threading.currentThread().getName()
        reason = "[%s] %s" % (threadname, reason)
        match = myDB.match("SELECT * from authors where AuthorID=?", (AuthorID,))
        if not match:
            logger.warn("Authorid %s not found in database, unable to add %s" % (AuthorID, bookname))
        else:
            controlValueDict = {"BookID": bookid}
            newValueDict = {
                "AuthorID": AuthorID,
                "BookName": bookname,
                "BookSub": booksub,
                "BookDesc": bookdesc,
                "BookIsbn": bookisbn,
                "BookPub": bookpub,
                "BookGenre": bookgenre,
                "BookImg": bookimg,
                "BookLink": booklink,
                "BookRate": bookrate,
                "BookPages": bookpages,
                "BookDate": bookdate,
                "BookLang": bookLanguage,
                "Status": bookstatus,
                "AudioStatus": audiostatus,
                "BookAdded": today(),
                "WorkID": workid,
                "ScanResult": reason,
                "OriginalPubDate": originalpubdate
            }

            myDB.upsert("books", newValueDict, controlValueDict)
            logger.info("%s by %s added to the books database, %s/%s" % (bookname, authorname, bookstatus, audiostatus))

            if 'nocover' in bookimg or 'nophoto' in bookimg:
                # try to get a cover from another source
                workcover, source = getBookCover(bookid)
                if workcover:
                    logger.debug('Updated cover for %s using %s' % (bookname, source))
                    controlValueDict = {"BookID": bookid}
                    newValueDict = {"BookImg": workcover}
                    myDB.upsert("books", newValueDict, controlValueDict)

            elif bookimg and bookimg.startswith('http'):
                link, success, _ = cache_img("book", bookid, bookimg)
                if success:
                    controlValueDict = {"BookID": bookid}
                    newValueDict = {"BookImg": link}
                    myDB.upsert("books", newValueDict, controlValueDict)
                else:
                    logger.debug('Failed to cache image for %s' % bookimg)

            serieslist = []
            if series:
                serieslist = [('', seriesNum, cleanName(series, '&/'))]
            if lazylibrarian.CONFIG['ADD_SERIES'] and "Ignored:" not in reason:
                newserieslist = getWorkSeries(workid, reason=reason)
                if newserieslist:
                    serieslist = newserieslist
                    logger.debug('Updated series: %s [%s]' % (bookid, serieslist))
                setSeries(serieslist, bookid, reason=reason)

            setGenres(getList(bookgenre, ','), bookid)

            worklink = getWorkPage(bookid)
            if worklink:
                controlValueDict = {"BookID": bookid}
                newValueDict = {"WorkPage": worklink}
                myDB.upsert("books", newValueDict, controlValueDict)
